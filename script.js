var ships_info_arr = new Array(10);

for(var i = 0; i< 10; i++){
	ships_info_arr[i] = new Array(10);
}

function getRandomInt(min,max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function isVerticalOrientation(){
	if(getRandomInt(0,1) == 1){
		return true;
	}
	else{
		return false;
	}
}

function cellsAroundIsEmpty(i,j){

	if(ships_info_arr[i][j] == true){ // center
		return false;
	}

	if(i == 0 || i == 9){ // first or last row

		if(j > 0){

			if(ships_info_arr[i][j-1] == true){ //left
				return false;
			}

			if(i == 0){

				if(ships_info_arr[i+1][j-1] == true){ //bottom-left
					return false;
				}
			}
			else{

				if(ships_info_arr[i-1][j-1] == true){ //top-left
					return false;
				}
			}

		}

		if(j < 9){

			if(ships_info_arr[i][j+1] == true){ // right
				return false;
			}

			if(i == 0){

				if(ships_info_arr[i+1][j+1] == true){ // bottom-right
					return false;
				}
			}
			else{

				if(ships_info_arr[i-1][j+1] == true){ // top-right
					return false;
				}
			}
		}

		if(i == 0){
			
			if(ships_info_arr[i+1][j] == true){ // bottom
				return false;
			}
		}
		else{
			
			if(ships_info_arr[i-1][j] == true){ //top
				return false;
			}
		}
	}

	if(j == 0 || j == 9){ // first or last column

		if(i > 0){

			if(ships_info_arr[i-1][j] == true){ // top
				return false;
			}

			if(j == 0){

				if(ships_info_arr[i-1][j+1] == true){ // top-right
					return false;
				}
			}
			else{
				if(ships_info_arr[i-1][j-1] == true){ // top-left
					return false;
				}
			}
		}

		if(i < 9){

			if(ships_info_arr[i+1][j] == true){ // bottom
				return false;
			}

			if(j == 0){

				if(ships_info_arr[i+1][j+1] == true){ // bottom-right
					return false;
				}
			}
			else{

				if(ships_info_arr[i+1][j-1] == true){ // bottom-left
					return false;
				}
			}
		}

		if(j == 0){

			if(ships_info_arr[i][j+1] == true){ // right
				return false;
			}
		}
		else{

			if(ships_info_arr[i][j-1] == true){ // left
				return false;
			}
		}
	}

	if(j != 0 && j != 9 && i != 0 && i != 9)
	{
		if(ships_info_arr[i-1][j-1] == true){ // top-left
			return false;
		}

		if(ships_info_arr[i-1][j] == true){ // top
			return false;
		}

		if(ships_info_arr[i-1][j+1] == true){ // top-right
			return false;
		}

		if(ships_info_arr[i][j-1] == true){ // left
			return false;
		}

		if(ships_info_arr[i][j+1] == true){ //right
			return false;
		}

		if(ships_info_arr[i+1][j-1] == true){ // bottom-left
			return false;
		}

		if(ships_info_arr[i+1][j] == true){ // bottom
			return false;
		}

		if(ships_info_arr[i+1][j+1] == true){ // bottom-right
			return false;
		}
	}

	return true;
}

function calculateOneCellShips(){
	var count = 0,
	iterations = 0;

	while(count < 4){

		iterations++;
		
		if(iterations > 50){
			return 'error';
		}
		
		var i = getRandomInt(0,9);
		var j = getRandomInt(0,9);

		if(cellsAroundIsEmpty(i,j)){
			ships_info_arr[i][j] = true;
			count++;
		}
	}
}

function calculateTwoCellShips(){

	var count = 0, 
	iterations = 0,
	verticalMargin = 0,
	horizontalMargin = 0;

	while(count < 3){

		iterations++;
		
		if(iterations > 50){
			return 'error';
		}

		if(isVerticalOrientation()){
			verticalMargin = 1;
			horizontalMargin = 0;
		}
		else{
			verticalMargin = 0;
			horizontalMargin = 1;
		}

		var i = getRandomInt(0 + verticalMargin, 9);
		var j = getRandomInt(0, 9 - horizontalMargin);

		if(cellsAroundIsEmpty(i,j)){

			if(verticalMargin != 0){

				if(cellsAroundIsEmpty(i-1,j)){
					ships_info_arr[i][j] = true;
					ships_info_arr[i-1][j] = true;
					count++;
				}
			}
			else{

				if(cellsAroundIsEmpty(i,j+1)){
					ships_info_arr[i][j] = true;
					ships_info_arr[i][j+1] = true;
					count++;
				}
			}

		}
	}
}

function calculateThreeCellShips(){

	var count = 0, 
	iterations = 0,
	verticalMargin = 0,
	horizontalMargin = 0;

	while(count < 2){

		iterations++;

		if(iterations > 50){
			return 'error';
		}

		if(isVerticalOrientation()){
			verticalMargin = 2;
			horizontalMargin = 0;
		}
		else{
			verticalMargin = 0;
			horizontalMargin = 2;
		}

		var i = getRandomInt(0 + verticalMargin, 9);
		var j = getRandomInt(0, 9 - horizontalMargin);

		if(cellsAroundIsEmpty(i,j)){

			if(verticalMargin != 0){

				if(cellsAroundIsEmpty(i-1,j) && cellsAroundIsEmpty(i-2,j)){
					ships_info_arr[i][j] = true;
					ships_info_arr[i-1][j] = true;
					ships_info_arr[i-2][j] = true;
					count++;
				}
			}
			else{

				if(cellsAroundIsEmpty(i,j+1) && cellsAroundIsEmpty(i,j+2)){
					ships_info_arr[i][j] = true;
					ships_info_arr[i][j+1] = true;
					ships_info_arr[i][j+2] = true;
					count++;
				}
			}

		}
	}
}

function calculateFourCellShip(){

	var count = 0,
	verticalMargin = 0,
	horizontalMargin = 0;

	while(count < 1){

		if(isVerticalOrientation()){
			verticalMargin = 3;
			horizontalMargin = 0;
		}
		else{
			verticalMargin = 0;
			horizontalMargin = 3;
		}

		var i = getRandomInt(0 + verticalMargin, 9);
		var j = getRandomInt(0, 9 - horizontalMargin);

		if(cellsAroundIsEmpty(i,j)){

			if(verticalMargin != 0){

				if(cellsAroundIsEmpty(i-1,j) && cellsAroundIsEmpty(i-2,j) && cellsAroundIsEmpty(i-3,j)){
					ships_info_arr[i][j] = true;
					ships_info_arr[i-1][j] = true;
					ships_info_arr[i-2][j] = true;
					ships_info_arr[i-3][j] = true;
					count++;
				}
			}
			else{

				if(cellsAroundIsEmpty(i,j+1) && cellsAroundIsEmpty(i,j+2) && cellsAroundIsEmpty(i,j+3)){
					ships_info_arr[i][j] = true;
					ships_info_arr[i][j+1] = true;
					ships_info_arr[i][j+2] = true;
					ships_info_arr[i][j+3] = true;
					count++;
				}
			}

		}
	}
}

function clearArray(){
	for(var i = 0; i< 10; i++){
		for(var j = 0; i< 10; j++){
			ships_info_arr[i][j] = null;
		}
	}
}

function calculateShips(){ // 'error' = too much time for calculating

	calculateFourCellShip();

	if(	calculateThreeCellShips() == 'error'){
		clearArray();
		calculateShips();
		return;
	}

	if(calculateTwoCellShips() == 'error'){
		clearArray();
		calculateShips();
		return;
	}
	if(calculateOneCellShips() == 'error'){
		clearArray();
		calculateShips();
		return;
	}
}

calculateShips();

jQuery('document').ready(function(){
	var combined_str;

	for(var i = 0; i< 10; i++){
		combined_str+='<tr>';
		for(var j = 0; j< 10; j++){
			if(ships_info_arr[i][j] == true)
				combined_str+='<td class="active"></td>';
			else
				combined_str+='<td></td>';
		}
		combined_str+='<tr/>';
	}

	jQuery('table').append(combined_str);
})